#!/usr/bin/env bash
# Example:
#
#  * cat my-user-data
#    #cloud-config
#    password: passw0rd
#    chpasswd: { expire: False }
#    ssh_pwauth: True
#  * echo "instance-id: $(uuidgen || echo i-abcdefg)" > my-meta-data
#  * cloud-localds my-seed.img my-user-data my-meta-data
#  * kvm -net nic -net user,hostfwd=tcp::2222-:22 \
#       -drive file=disk1.img,if=virtio -drive file=my-seed.img,if=virtio
#  * ssh -p 2222 ubuntu@localhost


#From Using Ubuntu Cloud Images w/o cloud]http://ubuntu-smoser.blogspot.com/2013/02/using-ubuntu-cloud-images-without-cloud.html)
# sudo apt-get install kvm cloud-utils genisoimage

# ## URL to most recent cloud image of 12.04
# $ img_url="http://cloud-images.ubuntu.com/server/releases/12.04/release"
# $ img_url="${img_url}/ubuntu-12.04-server-cloudimg-amd64-disk1.img"
# ## download the image
# $ wget $img_url -O disk.img.dist
# 
# ## Create a file with some user-data in it
# $ cat > my-user-data <<EOF
# #cloud-config
# password: passw0rd
# chpasswd: { expire: False }
# ssh_pwauth: True
# EOF
# 
# ## Convert the compressed qcow file downloaded to a uncompressed qcow2
# $ qemu-img convert -O qcow2 disk.img.dist disk.img.orig
# 
# ## create the disk with NoCloud data on it.
# $ cloud-localds my-seed.img my-user-data
# 
# ## Create a delta disk to keep our .orig file pristine
# $ qemu-img create -f qcow2 -b disk.img.orig disk.img
# 
# ## Boot a kvm
# $ kvm -net nic -net user -hda disk.img -hdb my-seed.img -m 512

sudo apt-get --yes install qemu-kvm libvirt-bin qemu-utils genisoimage virtinst
sudo mkdir /var/lib/libvirt/images/base
sudo mkdir /var/lib/libvirt/images/hosts
# wget https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img -O /var/lib/libvirt/images/base/bionic-server-cloudimg-amd64.img
sudo qemu-img create -f qcow2 -o backing_file=/var/lib/libvirt/images/base/bionic-server-cloudimg-amd64.img /var/lib/libvirt/images/hosts/root-rootdisk.img

cloud-localds my-seed.img my-user-data my-meta-data